<?php
/**
 * Stub Backend configuration stuff
 *
 * @todo Move this stuff to Drupal settings or to the relevant concrete class definition
 *
 * @package     lobby
 * @author      Peter Cruickshank
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
global $_BE;
// Note $_PSL is not used by the code - path and DB information is extracted from targetfinder/DO/BE_DO.ini
// through the constant EACTIONS_ROOT which is defined in ../config.php.

// define regular expressions to parse scraped data
/*
$_BE['AddressRegexps'] = array("/(?s)(.+)\n([-\w']+), ?([-\w']+)(?:, ?([-\w']+))?\n([A-Z][0-9][A-Z] ?[0-9][A-Z][0-9])(?:\n([-\w':]+)\n)?/"
=> array('','address','city','province','country','postalCode','country'),
"/^Telephone: *([0-9\(\)\- ]+)/m" => array('','telephone'),
"/^Fax: *([0-9\(\)\- ]+)/m" => array('','fax'),
"/^E-Mail: *([\w0-9@.-_]+)/m" => array('','email'),
);
*/
$_BE['AddressRegexps'] = array( 'phoneNumber' => "/^[Tt]el(ephone)?.?: *(\(?\d{3}\)?)?\s*\d{3}-?\d{4}/",
                                'faxNumber' => "/^[Ff]ax: *(\(?\d{3}\)?)?\s*\d{3}-?\d{4}/",
                                'tty' => "/^(tty|TTY): *(\(?\d{3}\)?)?\s*\d{3}-?\d{4}/",
                                'email' => "/(([_a-zA-Z0-9-+]+)(\.[_a-zA-Z0-9-+]+)*@(([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])+)(\.([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])+)*(\.[a-zA-Z]{2,4}))/"
                                );

$_BE['ReplaceRegExps'] = array ( 'phoneNumber' => "/[Tt]el(.|ephone)?: */", 
                                 'faxNumber' => "/[Ff]ax: */",
                                 'tty' => "/(tty|TTY): */" 
                                 );
// misc settings
$_BE['AddressDefaultCountry'] = 'Canada';
$_BE['ActionShowCounterMin']  = 100;
$_BE['ActionShowHitsMin']     = 50;

/**
*  Intializes DB_DataObjects
*
* From BE_functions.php
*/
function be_initializeDBDataObjects() {
  global $_PSL, $_DB_DATAOBJECT;

  if (!empty($_DB_DATAOBJECT['CONFIG'])) {
    return;
  }

  require_once 'PEAR.php';

  if (!defined('DB_DATAOBJECT_NO_OVERLOAD')) {
    define('DB_DATAOBJECT_NO_OVERLOAD',true); // This is set to TRUE somewhere else in Drupal/CiviCRM
  }

  $options = &PEAR::getStaticProperty('DB_DataObject','options');
  $config = parse_ini_file(EACTIONS_ROOT . '/targetfinder/DO/BE_DO.ini', true);

//  $db_name = $_PSL['DB_Database'];
  $db_name = $config['DB_DataObject']['database_name'];
  $config['DB_DataObject']["ini_".$db_name] = EACTIONS_ROOT . '/DO/be7.ini';
  //  $config['DB_DataObject']["ini_".$db_name] = 'DO/be7.ini';

  foreach($config as $class=>$values) {
    foreach ($values as $k=>$v) {
      //hackery because of the names
      @eval("\$v = \"$v\";");
      $values[$k] = $v;
    }
    $options = &PEAR::getStaticProperty($class,'options');
    $options = $values;
  }

}

/**
* Adapted for Drupal
* @return integer Uid of currently logged in Drupal user
*/
function be_getCurrentAuthorID() {
  global $user;
  return $user->uid;
}
?>
