<?php
/**
 * @file
 * Administration of Lobby targets (MPs, corporations etc)
 *
 * @package     lobby
 * @author      Peter Cruickshank
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

include_once 'Data/Target.php';

function target_help ($section = 'admin/help#target') {
  $output = '';

  switch($section) {
    case 'admin/help#target':
    $output .= t("
      <p>Users with the correct <a href=\"%permissions\">permissions</a> can administer targets used by the Lobbying system.</p>
      <p>To enable its use, a user needs the \"administer targets\" permission.</p>",
    array("%permissions" => url("admin/user/permission"), "%target" => url("target")));
    break;
    case 'admin/modules#description':
    $output = t("Enables administration of Lobbying targets.");
    break;
  }

  return $output;
}

function target_perm() {
  return array ('administer targets');
}

function target_menu() {
  $items = NULL;

  $admin_access = user_access('administer targets');

  $items[] = array(
  'path'     => 'admin/target/create',
  'callback' => 'target_edit',
  'callback arguments' => array('create'),
  'title'    => t('create target'),
  'access'   => $admin_access,
  'weight'   => -2);

  $items[] = array(
  'path'     => 'admin/target/edit',
  'callback' => 'target_edit',
  'callback arguments' => array('edit'),
  'title'    => t('target details'),
  'access'   => $admin_access,
  'weight'   => -4,
  'type'     => MENU_CALLBACK);

  $items[] = array(
  'path'     => 'admin/target/delete',
  'callback' => 'target_delete',
  'callback arguments' => array('delete'),
  'title'    => t('delete target'),
  'access'   => $admin_access,
  'weight'   => 5,
  'type'     => MENU_CALLBACK);

  $items[] = array(
  'path'     => 'admin/target',
  'callback' => 'target_list',
  'title'    => t('lobby targets'),
  'access'   => $admin_access,
  'weight'   => -5);

  //  }

  return $items;
}

/**
* Implementation of hook_link
*/
function target_link($type) {
  $links = array();
  if ($type == 'page' && user_access('administer targets')) {
    $links[] = l(t('Lobby targets'), 'target');
  }
  return $links;
}

/**
* Implementation of hook_settings
*/
function target_settings() {
  $form["target_logging"] = array(
		  '#type' => 'checkbox',
		  '#title' => t("Log all changes to watchdog"),
		  '#return_value' => 1,
		  '#default_value' => variable_get("target_logging", "1"),
		);

  return $form;
}


/**
* Menu callback
*
* This mixes DB access code with HTML formatting. Seems to be the Drupal way
*/
function target_list() {

  $lobby_target_types = variable_get('lobby_target_list',NULL);
  unset($_SESSION['target_edit']['tid']);
  unset($_SESSION['target_list']);

  $header = array(
  array('data' => t('Type'), 'field' => 't.contactType'),
  //  array('data' => t('Name'), 'field' => 't.displayName', 'sort' => 'asc'),
  array('data' => t('Name'), 'field' => 'SortName', 'sort' => 'asc'),
  array('data' => t('Organization'), 'field' => 't.companyName'),
  array('data' => t('Email'), 'field' => 't.email'),
  array('data' => t('Fax'), 'field' => 't.faxNumber'),
  array('data' => t('Last Changed'), 'field' => 't.dateModified'),
  array('data' => t('By'), 'field' => 'u.name'),
  ''
  );

  $sql = "SELECT t.*, CONCAT(t.lastName, ', ', t.firstName) as SortName, u.name FROM {be_contact} t LEFT OUTER JOIN {users} u ON t.author_id = u.uid "
  . tablesort_sql($header);
  $result = pager_query($sql, 20);

  while ($target = db_fetch_object($result)) {
    $rows[] = array('data' =>
    array(
    // Cells
    $lobby_target_types[$target->contactType],
    truncate_utf8($target->displayName, 64),
    truncate_utf8($target->companyName, 64),
    truncate_utf8($target->email, 64),
    truncate_utf8($target->faxNumber, 64),
    $target->dateModified == 0 ? '' : format_date($target->dateModified, 'small'),
    $target->author_id == 0 ? '' : theme('username', $target),
    l(t('edit'), "admin/target/edit/$target->contactID"),
    l(t('delete'), "admin/target/delete/$target->contactID"),
    ) );
    $_SESSION['target_list'][$target->contactID] = $target->displayName;
  }

  if (!$rows) {
    $rows[] = array(array('data' => t('No targets available.'), 'colspan' => '8'));
  }

  $pager = theme('pager', NULL, 20, 0);
  if (!empty($pager)) {
    $rows[] = array(array('data' => $pager, 'colspan' => '7'));
  }

  $output = theme('table', $header, $rows);

  print theme('page', $output);

}

/**
* Menu callback
*/
function target_edit($action) {

  $output = '';

  if ($action == 'create') {
    $values = array();
    $form['create'] = array(
		  '#type' => 'submit',
		  '#value' => t('Create'),
		);
  }
  else {
    $form['update'] = array(
		  '#type' => 'submit',
		  '#value' => t('Update'),
		);
  }

  if ($_POST['edit']) {
    // Validate and save submitted form
    $values = $_POST['edit'];
    $tid = $values['tid'] = (!empty($_SESSION['target_edit']['tid'])) ? $_SESSION['target_edit']['tid'] : arg(3); // Target ID from the session - can't trust POST
    if (!_target_validate_data($values)) {
      watchdog('lobby', t('There are errors in your form'), WATCHDOG_ERROR);
      drupal_set_message(t('There are errors in your form'));
    }
    else {
      unset($_SESSION['target_edit']['tid']);

      $isOk = $values['tid'] = _target_save_database($values);
      if (!$isOk) {
        watchdog('lobby', t('Problem saving target values'), WATCHDOG_ERROR);
        drupal_set_message(t('Problem saving target values'), 'error');
      } else {
        _target_watchdog_log($values);
        drupal_set_message(t('Target values saved'));
        drupal_goto('/admin/target');
        exit(0);
      }
    }

  }
  elseif ($action == 'edit') {
    // Load and present in a form data on this target
    $tid = arg(3);
    $values = _target_load_database($tid);
    if (!$values) {
      unset($_SESSION['target_edit']['tid']); // In case an old value is lurking around
      watchdog('lobby', 'Database failure reading target', WATCHDOG_ERROR);
      drupal_set_message(t("Database failure reading target #%d",$tid),'error');
      drupal_goto('/admin/target');
    }
    $_SESSION['target_edit']['tid'] = $values['tid']; // Save for the return trip
  }
  else {
    // Creating a new target - no default values (for now at least)
    $values = array();
    unset($_SESSION['target_edit']['tid']); // In case an old value is lurking around
  }

  $output = _target_display_form($values, $form);

  print theme('page', $output);
}

/**
* Menu callback
*/
function target_delete() {

  // if ($_POST['confirm']) {
    $tid = arg(3);
    Data_Target::delete($tid);
    drupal_goto('admin/target');
  /* form's not working
  } else {
    return drupal_get_form('target_confirm_delete', $_SESSION['target_list'][$tid], $tid);
  }
  */
}

/*
function target_confirm_delete($name, $uid) {
  return confirm_form(array(),
    t('Are you sure you want to delete the target %name?', array('%name' => $name)), 'admin/target/'. $uid,
    t('This will disable all actions that use this target. This action cannot be undone.'), t('Delete'), t('Cancel'));
}
*/

/**
* Note that target's ID is kept in the session - we cant trust POST to preserve it
*
* @access private
* @return htmlstring
*/
function _target_display_form($values, $button='') {
  $title = t('Target maintentance');
  $message = t('This is aimed at the capture of the basic information required to contact the target electronically. It will eventually be merged into the full CiviCRM system, allowing more sophisticated information management');
  if (empty($button)) {
    $button[] = array(
		  '#type' => 'submit',
		  '#value' => t("Create"),
		);
  }

  $form = '';

	$form['target'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Individual Target'),
	);

  // tid in session
  // $form['target']['tid'] = array('#type' => 'hidden', '#value' => $values['tid']);

  $form['target']['prefix'] = array(
	  '#type' => 'textfield',
	  '#title' => t("Title"),
	  '#default_value' => $values['prefix'],
	  '#size' => 5,
	  '#maxlength' => 10,
	);

  $form['target']['first_name'] = array(
	  '#type' => 'textfield',
	  '#title' => t('First Name'),
	  '#default_value' => $values['first_name'],
	  '#size' => 20,
	  '#maxlength' => 30,
	);

  $form['target']['last_name'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Last Name'),
	  '#default_value' => $values['last_name'],
	  '#size' => 20,
	  '#maxlength' => 40,
	);

	$form['target']['salutation'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Salutation'),
	  '#default_value' => $values['salutation'],
	  '#size' => 20,
	  '#maxlength' => 40,
	);

	  $form['organization'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Organizational Information'),
	);

  $form['organization']['organization_name'] = array(
	  '#type' => 'textfield',
	  '#title' => t("Organization"),
	  '#default_value' => $values['organization_name'],
	  '#size' => 20,
	  '#maxlength' => 30,
	);

  $form['organization']['city'] = array(
	  '#type' => 'textfield',
	  '#title' => t('City'),
	  '#default_value' => $values['city'],
	  '#size' => 20,
	  '#maxlength' => 30,
	);

  $lobby_target_types = variable_get('lobby_target_list',NULL);

  $form['contact_methods'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Contact methods'),
	);

  $form['contact_methods']['ttid'] = array(
	  '#type' => 'select',
	  '#title' => t("Type"),
	  '#default_value' => $values['ttid'],
	  '#options' => $lobby_target_types,
	);

  $form['contact_methods']['email'] = array(
	  '#type' => 'textfield',
	  '#title' => t("Email"),
	  '#default_value' => $values['email'],
	  '#size' => 30,
	  '#maxlength' => 64,
	);

  $form['contact_methods']['phone'] = array(
	  '#type' => 'textfield',
	  '#title' => t("Phone"),
	  '#default_value' => $values['phone'],
	  '#size' => 15,
	  '#maxlength' => 20,
	);

  $form['contact_methods']['fax'] = array(
	  '#type' => 'textfield',
	  '#title' => t("Fax"),
	  '#default_value' => $values['fax'],
	  '#size' => 15,
	  '#maxlength' => 20,
	);

  $form["referer"] = array(
	  '#type' => 'hidden',
	  '#value' => $_SERVER['HTTP_REFERER'],
	);

//  $form .= $button;
	array_push($form, $button);
	$form['#method'] = 'post';
  $output = theme('box', $title, $message . drupal_get_form('target_display_form', $form));

  return $output;
}

/**
* Record creation/editing of target
* @todo Record action, and target's contactID
* @access private
*/
function _target_watchdog_log($values) {
  $tid   = $values["tid"];
  $name  = $values["display_name"];
  $email = $values["email"] ;
  $fax   = $values["fax"] ;
  $message = "target #$tid changed to: $name <$email> Fax: $fax";
  watchdog('lobby', $message, WATCHDOG_NOTICE);
}

/**
* @access private
* @param Array Values from form
* @return Boolean True if data is valid
*/
function _target_validate_data(&$values) {

  foreach ($values as $k => $v) {
    $values[$k] = trim($v);
  }
  $isOk = TRUE;

  if (empty($values['first_name']) && empty($values['last_name'])) {
    if (empty($values['organization_name']) || empty($values['city'])) {
      form_set_error('organization_name',t('We either need an organization name and city, or a first and last name'));
      form_set_error('city','');
      $isOk = FALSE;
    }
    else {
      $values['display_name'] = $values['organization_name'] .' '. $values['city'];
    }
  }
  elseif (empty($values['first_name']) || empty($values['last_name'])) {
    form_set_error('first_name',t('We need both a first and last name'));
    form_set_error('last_name','');
    $isOk = FALSE;
  }
  else {
    $values['display_name'] = $values['prefix'] .' '. $values['first_name'] .' '. $values['last_name'];
  }

  if (empty($values['saluation'])) {
    $values['salutation'] = $values['prefix'] .' '. $values['last_name'];
  }

  if (empty($values['email'])) {
    if (empty($values['fax'])) {
      form_set_error('email',t('We need either an email or a fax number for this target'));
      form_set_error('fax','');
      $isOk = FALSE;
    }
  }
  elseif (!valid_email_address($values['email'])) {
    form_set_error('email',t('Email address is not valid'));
    $isOk = FALSE;
  }

  return $isOk;
}

/**
* Load and format the target record
*
* @access private
* @param Integer
* @return Array/FALSE
*/
function _target_load_database($tid) {

  $target = Data_Target::load($tid);
  if (!$target) {
    return FALSE;
  }

  $values = NULL;
  $values['tid']                = $target->tid;
  $values['ttid']               = $target->ttid;
  $values['display_name']       = $target->display_name;
  $values['prefix']             = $target->prefix;
  $values['first_name']         = $target->first_name;
  $values['last_name']          = $target->last_name;
  $values['salutation']         = $target->salutation;
  $values['organization_name']  = $target->organization_name;
  $values['city']               = $target->city;
  $values['email']              = $target->email;
  $values['phone']              = $target->phone;
  $values['fax']                = $target->fax;

  return $values;
}

/**
* Create or update Target record, depending on whether the target id is known.
*
* @return Integer/FALSE Target ID if action was successful
* @access private
*/
function _target_save_database($values) {
  global $user;
  $values['uid'] = $user->uid;
  if ($tid = $values['tid']) {
    Data_Target::update($tid, $values);
  }
  else {
    $tid = Data_Target::create($values);
  }
  return $tid;
}
?>
