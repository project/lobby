<?php
/**
 * @file
 * Static data access class
 * Mostly acting on {eaction_lobby} but including associated pivot tables
 *   {eaction_lobby_targetfinder}
 *   {eaction_lobby2participant}
 *   {eaction_lobby_participant_target}
 * Could probably be replaced by a PEAR::DataObject, but that just seems to be extra overhead
 * when Drupal already has a DB abstraction layer
 *
 *
 * @package     lobby
 * @author      Peter Cruickshank
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
//include_once 'Data/Target.php';

#define('EACTION_LOBBY_TARGET_DIRECT',0);
class Data_Lobby {

  /**
  * @return integer
  */
  function access_count($nid) {
    return db_result(db_query('SELECT count(*) FROM {eaction_lobby2participant} WHERE nid = %d',$nid));
//    return db_result(db_query('SELECT action_counter FROM {eaction_lobby} WHERE nid = %d',$nid));
  }
  function increment_access_count($nid) {
    db_query('UPDATE {eaction_lobby} SET action_counter=action_counter+1 WHERE nid = %d',$nid);
  }

  /**
  * Returns Full details of this lobby item
  * Including list of permitted target_types
  *
  * @return Object
  */
  function load($nid) {
    $item = db_fetch_object(db_query('SELECT * FROM {eaction_lobby} WHERE nid = %d', $nid));
    $item->action_counter = Data_Lobby::access_count($nid);
    $item->target_types = Data_Lobby::load_target_types($nid);
    $item->direct_targets = Data_Lobby::load_direct_targets($nid);
    return $item;
  }

  /**
  * Requires that dates have been turned into unix timestamps
  */
  function update($node) {
    db_query("UPDATE {eaction_lobby} SET method=%d, action_counter=%d, message='%s', can_comment='%s', comment='%s', thank_you='%s', target_url='%s' WHERE nid=%d",
    $node->method, $node->action_counter, $node->message, $node->can_comment, $node->comment, $node->thank_you, $node->target_url, $node->nid);
    Data_Lobby::insert_target_types($node->nid, $node->target_types);
    Data_Lobby::insert_direct_targets($node->nid, $node->direct_targets);
  }

  function insert($node) {
    $q = "INSERT INTO {eaction_lobby} (nid,method,action_counter, message, can_comment,comment,thank_you, target_url)
                 VALUES (%d, %d, %d, '%s', '%s', '%s', '%s', '%s')";
    db_query($q,
    $node->nid, $node->method, $node->action_counter, $node->message, $node->can_comment, $node->comment, $node->thank_you, $node->target_url);
    Data_Lobby::insert_target_types($node->nid, $node->target_types);
    Data_Lobby::insert_direct_targets($node->nid, $node->direct_targets);
  }

  /**
  * @param Integer nid of affected node
  */
  function delete($node) {
    Data_Lobby::delete_direct_targets($node->nid);
    Data_Lobby::delete_target_types($node->nid);
    db_query('DELETE FROM {eaction_lobby} WHERE nid = %d', $node->nid);
  }


  // DIRECT TARGETS

  /**
  * @return Array Listing all the direct targets for this Lobby
  */
  function load_direct_targets($nid) {
    $r = db_query('SELECT t.* FROM {eaction_lobby2target} lt INNER JOIN {be_contact} t ON t.contactID = lt.tid  WHERE lt.nid=%d AND lt.ttid=0 ORDER BY t.lastName, t.firstName',$nid);
    $out = NULL;
    while ($row = db_fetch_object($r)) {
      $out[] = Data_Target::crm_fields($row);
//      $out[] = $row;
    }
    return $out;
  }

  /**
  * @param Integer nid of affected node
  * @param Array target ids
  */
  function insert_direct_targets($nid, $targets) {
    Data_Lobby::delete_direct_targets($nid);
    if (is_array($targets)) {
      foreach ($targets as $v) {
        db_query('INSERT INTO {eaction_lobby2target} (nid,ttid,tid) VALUES (%d,0,%d)',$nid,$v);
      }
    }
  }

  // TARGET TYPES

  /**
  * @param Integer nid of affected node
  */
  function delete_direct_targets($nid) {
    db_query('DELETE FROM {eaction_lobby2target} WHERE nid=%d AND ttid=0',$nid);
  }

  /**
  * @return Array listing all the target types for this EAction_Lobby
  */
  function load_target_types($nid) {
    $out = NULL;
    $r = db_query('SELECT ttid FROM {eaction_lobby2targettype} WHERE nid=%d', $nid);
    while ($row = db_fetch_object($r)) {
      $out[] = $row->ttid;
    }
    return $out;
  }

  function insert_target_types($nid, $types) {
    Data_Lobby::delete_target_types($nid);
    if (is_array($types)) {
      foreach ($types as $v) {
      	if ($v != 0) {
        	db_query('INSERT INTO {eaction_lobby2targettype} (nid,ttid) VALUES (%d,%d)',$nid,$v);
      	}
      }
    }
  }

  /**
  * @param Integer nid of affected node
  */
  function delete_target_types($nid) {
    db_query('DELETE FROM {eaction_lobby2targettype} WHERE nid=%d',$nid);
  }


  // LINKS BETWEEN LOBBY AND PARTICIPANT

  /**
  * Get the given participant for this lobby
  * @return array of objects
  */
  function load_participant_lobby($nid, $participantId) {
    if (empty($nid) || empty($participantId)) {
      return FALSE;
    }
    $target = db_fetch_object(db_query('SELECT * FROM {eaction_lobby2participant} WHERE nid=%d AND contact_id=%d', $nid, $participantId));
    return $target;
  }

  /**
  * Create a new link between a participant and the Lobby
  * @param array key(nid, contact_id)
  * @param array Field name/value pairs
  * @return array of objects
  */
  function create_participant_lobby($key, $values) {
    if (count($key) != 2 || empty($key[0]) || empty($key[1])) {
      return FALSE;
    }
#    $nid, $participantId
    $values['created'] or $values['created'] = time();
    return db_query('INSERT INTO {eaction_lobby2participant} (nid, contact_id, created, comment) VALUES (%d, %d, %d, \'%s\')', $key[0], $key[1], $values['created'], $values['comment']);
  }

  /**
  * @param array Key (nid, contact_id, ttid, tid)
  * @param array Field name/value pairs
  * @return Boolean
  */
  function update_participant_lobby($key, $values) {
    if (count($key) != 2) {
      return false;
    }
    $valueString = '';
    $valueArray = $key;
    foreach ($values as $k => $v) {
      $valueString .= "$k='%s', ";
      $valueArray[] = $v;
    }
    $valueString = substr($valueString,0,-2);
    return call_user_func_array(db_query('UPDATE {eaction_lobby2participant} SET $valueString WHERE nid=%d AND contact_id=%d',$valueArray));

  }

  // LINKS BETWEEN PARTICIPANT/ACTION AND TARGETS

  /**
  * List linking the given participant to this lobby's targets
  * @param array key(nid, contact_id)
  * @return array of objects
  */
  function load_participant_targets($key) {
    $nid          =$key[0];
    $participantId=$key[1];
    $out          = NULL;

    $r = db_query('SELECT * FROM {eaction_lobby2participant_target} WHERE nid=%d AND contact_id=%d', $nid, $participantId);
    while ($target = db_fetch_object($r)) {
      $out[] = $target;
    }
    return $out;
  }

  /**
   * Pull out linking record based on target id, or target type
   *
   * @param array key(nid, contact_id)
   * @param integer $participantId
   * @param integer $ttid
   * @param integer $tid
   * @return object/FALSE
   */
  function load_participant_target($key, $values) {
    $nid          = $key[0];
    $participantId= $key[1];
    $tid          = @$values['tid'];
    $ttid         = @$values['ttid'];

    assert('!empty($nid) && !empty($participantId)');
    assert('!empty($tid) || !empty($ttid)');

    if (!empty($ttid)) {
      $idCond = 'ttid=%d';
      $id = $ttid;
    }
    else {
      $idCond = 'tid=%d';
      $id = $tid;
    }

    $target = db_fetch_object(db_query('SELECT * FROM {eaction_lobby2participant_target} WHERE nid=%d AND contact_id=%d AND '.$idCond, $nid, $participantId, $id));
    return $target;
  }
  /**
  * @param array Key (nid, contact_id, tid, ttid) or (nid, contact_id, tid)
  * @param array Field name/value pairs [last_checked, status]
  * @return Boolean
  */
  function insert_participant_target($key, $values) {
    if (count($key) < 3) {
      return false;
    }
    $valueString = 'nid=%d, contact_id=%d, tid=%d, ';
    if (count($key) == 4) {
      $valueString .= 'ttid=%d, ';
    }
    $valueArray = $key;
    foreach ($values as $k => $v) {
      $valueString .= "$k='%s', ";
      $valueArray[] = $v;
    }
    $valueString = substr($valueString,0,-2);
    array_unshift($valueArray, 'INSERT INTO {eaction_lobby2participant_target} SET ' . $valueString);
    return call_user_func_array('db_query',$valueArray);
  }

  /**
  * @param array Key (nid, contact_id, tid, ttid)
  * @param array Field name/value pairs
  * @return Boolean
  */
  function update_participant_target($key, $values) {
    assert('count($key) == 4');
    $valueString = '';
    $valueArray = NULL;
    foreach ($values as $k => $v) {
      $valueString .= "$k='%s', ";
      $valueArray[] = $v;
    }
    $valueString = substr($valueString,0,-2);
    $keyString = 'nid=%d AND contact_id=%d AND tid=%d AND ttid=%d ';
		
    array_unshift($valueArray,"UPDATE {eaction_lobby2participant_target} SET $valueString WHERE $keyString");
    $valueArray = array_merge($valueArray,$key);

    //
    //die(var_dump($valueArray));
    //
    return call_user_func_array('db_query',$valueArray);
  }


}

?>
