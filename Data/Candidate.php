<?php
/**
 * @file
 * Static class providing DB access for Canadian candidate info
 *
 * The functions here could be later amended to use crm_*_contact calls
 *
 * @package     lobby
 * @author      Peter Cruickshank
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

class Data_Candidate {


  /**
   * Pull out ordered list of all candidates (for given riding)
   *
   * @todo Allow for pagination
   * @return Array
   */
  function list_all($edid=NULL) {
//    $r = db_query('SELECT * FROM {eaction_CA_candidates} WHERE edid=%d ORDER BY lastName, firstName', $edid);
//    $out = NULL;
//    while ($row = db_fetch_object($r)) {
//      $out[] = Data_Candidate::crm_fields($row);
//    }
    $out = NULL;
    $out->tid          = 111;
    $out->display_name = 'Hon Pierre Trudeau';
    $out->gender_id    = 2;
    $out->prefix       = 'Hon';
    $out->first_name   = 'Pierre';
    $out->last_name    = 'Trudeau';
    $out->salutation   = 'M Trudeau';
    $out->organization_name  = 'Liberal Party';
    $out->city         = 'Montreal';
    $out->email        = 'pierre@cruickshank.biz';
    $out->fax          = '1112224444';
    $out->phone        = '5556669999';
    $out->target_type  = 4;
    $out->postal_code  = 'E3H6N7';
    $out->edit         = '10001';
    $ret[] = $out;

    $out->tid          = 222;
    $out->display_name = 'Lord Conrad Black';
    $out->gender_id    = 2;
    $out->prefix       = 'Lord';
    $out->first_name   = 'Conrad';
    $out->last_name    = 'Black';
    $out->salutation   = 'Lord Black';
    $out->organization_name  = 'Conservative Party';
    $out->city         = 'Toronto';
    $out->email        = 'conrad@cruickshank.biz';
    $out->fax          = '1113334444';
    $out->phone        = '5556669999';
    $out->target_type  = 4;
    $out->postal_code  = 'K3H6N7';
    $out->edit  = '10001';
    $ret[] = $out;

    return $ret;
  }

  /**
  * Pull in details of given candidate
  * @todo Pull in contactType names as well as IDs?
  * @return object
  */
  function load($tid) {
    $r = db_query('SELECT * FROM {eaction_CA_candidates} WHERE id=%d',$tid);
    $out = Data_Target::crm_fields(db_fetch_object($r));
    return $out;
  }



  /**
  * Maps between CiviCRM and BE_Contact fieldnames
  * @todo Record candidate's id
  * @see CRM::useful_fields for equivalent mapping from CRM objects
  *
  * @param object BE_candidateFinder*
  * @return object
  */
  function crm_fields($candidate) {

    static $genderMap = array('U' => 0, 'F' => 1, 'M' => 2);
    $out = NULL;
    $out->tid          = $candidate->contactID;
    $out->display_name = $candidate->displayName;
    $out->gender_id    = $genderMap[$candidate->gender] or $out->gender_id = 0;
    $out->prefix       = $candidate->title; // Need to do prefix_id for CRM too?
    $out->first_name   = $candidate->firstName;
    $out->last_name    = $candidate->lastName;
    $out->salutation   = $candidate->salutation;
    $out->organization_name  = $candidate->companyName; // or $out->organization_name  = $candidate->displayName;
    $out->city         = $candidate->city;
    $out->email        = $candidate->email;
    $out->fax          = $candidate->faxNumber;
    $out->phone        = $candidate->phoneNumber;
    $out->candidate_type  = $candidate->candidateType;
    $out->postal_code  = $candidate->postalCode;

    return $out;
  }
}

?>
