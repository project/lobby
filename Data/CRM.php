<?php
/**
 * @file
 * DB access for CiviCRM tables
 *
 * Could probably be replaced by a PEAR::DataObject, but that just seems to be extra overhead
 * when Drupal already has a DB abstraction layer
 *
 *
 * @package     lobby
 * @author      Peter Cruickshank
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
class Data_CRM {

  /**
  * Given a Drupal user id, return the CiviCRM contact_id if there's a linked user
  *
  * @param integer Drupal user id
  * @return integer
  */
  function contact_id($uid) {
    return db_result(db_query('SELECT contact_id FROM {civicrm_uf_match} WHERE uf_id=%d',$uid));
  }

  /**
* This function shows how to pull out useful values from the CRM object's properties
* @todo Convert gender_id and prefix_id into strings?
* @todo Pull out EDID info **** (and other custom fields)
* @return Object
*/
  function useful_fields($CRM) {
    $prefixes = CRM_Core_PseudoConstant::individualPrefix();

    $out = NULL;
    $out->contact_id    = $CRM->id;
    $out->display_name  = $CRM->display_name;
    $out->gender_id     = $CRM->contact_type_object->gender_id;
//    $out->gender        = $CRM->contact_type_object->gender
//      or $out->gender        = $genderMap[$CRM->contact_type_object->gender_id];
    $out->prefix        = $CRM->contact_type_object->prefix
      or $out->prefix        = @$prefixes[$CRM->contact_type_object->prefix_id];
    $out->prefix_id     = $CRM->contact_type_object->prefix_id;
    $out->first_name    = $CRM->contact_type_object->first_name;
    $out->last_name     = $CRM->contact_type_object->last_name;
    $out->city          = $CRM->location[1]->address->postal_code;
    $out->state_province_id  = $CRM->location[1]->address->state_province_id;
    $out->state_province   = $CRM->location[1]->address->state_province
      or $out->state_province   = CRM_Core_PseudoConstant::stateProvince($CRM->location[1]->address->state_province_id);
    $out->country_id  = $CRM->location[1]->address->country_id;
    $out->country   = $CRM->location[1]->address->country
      or $out->country   = CRM_Core_PseudoConstant::country($CRM->location[1]->address->country_id);
    $out->postal_code   = $CRM->location[1]->address->postal_code;
    $out->email         = $CRM->location[1]->email[1]->email;

    $out->edit          = '10001'; //**** for testing
    return $out;
  }
}
