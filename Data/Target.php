<?php
/**
 * @file
 * DB access for be_contact tables where the BE_DO class isn't around
 *
 * The functions here could be later amended to use crm_*_contact calls
 *
 * @package     lobby
 * @author      Peter Cruickshank
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

class Data_Target {
  /**
   * Pull out ordered list of all targets
   *
   * @todo Allow for pagination
   * @return Array
   */
  function list_all() {
    $r = db_query('SELECT * FROM {be_contact} ORDER BY lastName, firstName');
    $out = NULL;
    while ($row = db_fetch_object($r)) {
      $out[] = Data_Target::crm_fields($row);
    }
    return $out;
  }

  /**
  * Pull in details of given target
  * @todo Pull in contactType names as well as IDs?
  * @return object
  */
  function load($tid) {
    $r = db_query('SELECT * FROM {be_contact} WHERE contactID=%d',$tid);
    $out = Data_Target::crm_fields(db_fetch_object($r));
    return $out;
  }

  /**
   * Create a new target record
   *
   * @param array $values
   */
  function create($values) {
    $now = time();
    $r = db_query('INSERT INTO {be_contact} SET contactType=%d, displayName=\'%s\', title=\'%s\', firstName=\'%s\', lastName=\'%s\', salutation=\'%s\', companyName=\'%s\', city=\'%s\', email=\'%s\', phoneNumber=\'%s\', faxNumber=\'%s\', dateCreated=%d, dateModified=%d, author_id=%s',
    $values['ttid'], $values['display_name'], $values['prefix'], $values['first_name'], $values['last_name'], $values['salutation'], $values['organization_name'], $values['city'], $values['email'], $values['phone'], $values['fax'], $now, $now, $values['uid']);
    return $r;
  }

  /**
   * Update a target record
   *
   * @param integer $tid
   * @param array $values
   */
  function update($tid, $values) {
    $now = time();
    $r = db_query('UPDATE {be_contact} SET contactType=%d, displayName=\'%s\', title=\'%s\', firstName=\'%s\', lastName=\'%s\', salutation=\'%s\', companyName=\'%s\', city=\'%s\', email=\'%s\', phoneNumber=\'%s\', faxNumber=\'%s\', dateModified=%d, author_id=%s WHERE contactID=%d',
    $values['ttid'], $values['display_name'], $values['prefix'], $values['first_name'], $values['last_name'], $values['salutation'], $values['organization_name'], $values['city'], $values['email'], $values['phone'], $values['fax'], $now, $values['uid'], $tid);
    return $r;
  }

  /**
   * Delete a target record
   *
   * @param integer $tid
   */
  function delete($tid) {
    $r = db_query('DELETE FROM {be_contact} WHERE contactID=%d', $tid);
    return $r;
  }

  /**
  * Maps between CiviCRM and BE_Contact fieldnames
  * @todo Record target's id
  * @see CRM::useful_fields for equivalent mapping from CRM objects
  *
  * @param object BE_TargetFinder*
  * @return object
  */
  function crm_fields($target) {

    static $genderMap = array('U' => 0, 'F' => 1, 'M' => 2);
    $out = NULL;
    $out->tid          = $target->contactID;
    $out->display_name = $target->displayName;
    $out->gender_id    = $genderMap[$target->gender] or $out->gender_id = 0;
    $out->prefix       = $target->title; // Need to do prefix_id for CRM too?
    $out->first_name   = $target->firstName;
    $out->last_name    = $target->lastName;
    $out->salutation   = $target->salutation;
    $out->organization_name  = $target->companyName; // or $out->organization_name  = $target->displayName;
    $out->city         = $target->city;
    $out->email        = $target->email;
    $out->fax          = $target->faxNumber;
    $out->phone        = $target->phoneNumber;
    $out->target_type  = $target->targetType;
    $out->postal_code  = $target->postalCode;

    return $out;
  }
}

?>
