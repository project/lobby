<?php
/**
* BE_Fax.class
*
* Fax utility for Back-End that works with the MyFax fax engine
* (www.myfax.com)
*
* @package     lobby
* @author      Marc-Antoine Parent
* @copyright   Copyright (C) 2003 OpenConcept Consulting
*
* This file is part of Back-End.
*
* Back-End is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* Back-End is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Back-End; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

require_once 'Fax.php';

class Fax_MyFax extends Fax {


  /**
   * Send the text message to the specified rececipient fax number
   *
   * MyFax uses a formatted email address to control options
   * fax=faxnumber/option=value@MyFax.com
   *
   * The fax formatting options are:
   * /rf=true|false  Fine image resolution
   * /le=true|false  Legal paper size
   * /ls=true|false Landscape setting
   * /cp=true|false  Cover page
   * /fr=fromname
   * /to=toname
   * /cf=URICA (any combination)
   * Where U = Urgent, R = For Review, I = For Information, C = Please Comment, and A = Please Reply - ASAP
   *
   * Subject goes in email subject line
   * Comments in email body
   * Billing code (arbitrary):
   * /bc=alphanumericbillingcode
   *
   *
   * @param string $toAddress Target's fax number
   * @param String $textMessage Body of email to send
   * @param array  (title, from, to_name etc)
   * @return Boolean Success of sending email to MyFax
   */
  function send($toFax, $textMessage, $param = NULL) {

    $options = '';
//    $options = '/rf=true/cp=false/cf=U';
    if ($param['from_name']) {
      $options .= '/fr='.$param['from_name'];
    }
    if ($param['to_name']) {
      $options .= '/to='.$param['to_name'];
    }
    if ($param['billing_code']) {
      $options .= '/bc='.$param['billing_code'];
    }

    if ($options) {
      if (EACTIONS_FAX_SPACEALLOWED !== TRUE) {
        $options = str_replace(' ','_',$options);
      }
      $toFax = 'fax='.$toFax;
    }
    $toAddress = clean_to( $toFax . $options ) . '@MyFax.com';

    if (defined('EACTIONS_FAX_PASSWORD')) {
      $textMessage = EACTIONS_FAX_PASSWORD. "\n$textMessage";
    }

    $headers = array(
    'Subject'       => $param['title'],
    'To'            => $toAddress,
    'From'          => $param['from_name'] .' <'.EACTIONS_FAX_SENDER.'>',
    'Reply-To'      => EACTIONS_FAX_SENDER,
    'Return-Path'   => EACTIONS_FAX_SENDER,
    # 'Cc'            => 'support@myfax.com',
    # 'Bcc'           => 'name@example.com'
    );

    $body = $textMessage;

    return eactions_mail($body, $headers, @$param['files']);

  }

}
