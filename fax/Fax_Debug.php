<?php
/**
* BE_Fax.class
*
* Fax utility for debugging: The Fax is emailed to EACTIONS_DEBUG_EMAIL
*
* @package     lobby
* @author      Marc-Antoine Parent
* @copyright   Copyright (C) 2003 OpenConcept Consulting
*
* This file is part of Back-End.
*
* Back-End is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* Back-End is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Back-End; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

require_once 'Fax.php';

class Fax_Debug extends Fax {

  /**
   * Return cleaned, validated fax number; return false if invalid
   *
   * Currently, only North American (10 digit) codes are accepted
   *
   * @todo Location-specific validation - Maybe use http://www.webservicex.net/country.asmx?op=GetISD
   * @todo Deal with special codes like *70
   *
   * @param String $faxNumber
   * @param Array $params Used for location-specific validation eg 'city', 'province', 'country', 'postal_code'
   * @return String/FALSE
   */
  function clean($faxNumber, $params=NULL) {
    //    $faxNumber = ereg_replace("[^0-9]", "", $faxNumber); // only keep numbers
    $faxNumber = parent::clean($faxNumber);

    if (strlen($faxNumber) != 10) {
      return FALSE;
    }

    // add default country code
    return '1'.$faxNumber;
  }


  /**
   * Send the text message to the specified rececipient fax number
   *
   * @param string $toAddress Target's fax number
   * @param Striing $textMessage Body of email to send
   * @param String $title
   * @param String $from
   * @param String $to
   * @param String $billingCode
   * @return Boolean Success of sending email to MyFax
   */
  function send($toAddress, $textMessage, $title='', $from = '', $to = '', $billingCode = '') {
    //    if (!defined(EACTIONS_DEBUG_EMAIL)) {
    //      return FALSE;
    //    }

    if (EACTIONS_FAX_SPACEALLOWED) {
      if ($from) {
        $toAddress .= "/fr=$from";
      }
      if ($to) {
        $toAddress .= "/to=$to";
      }
    }
    if ($billingCode) {
      $toAddress .= "/bc=$billingCode";
    }
    if ($toAddress) {
      $toAddress = "fax=$toAddress";
    }
    $toAddress .= "@MyFax.com";
    if (defined(EACTIONS_FAX_PASSWORD)) {
      $textMessage = EACTIONS_FAX_PASSWORD. "\n$textMessage";
    }

    $headers = array(
    'Subject'       => $title,
    'To'            => EACTIONS_DEBUG_EMAIL,
    'From'          => $from .' <'.EACTIONS_DEBUG_EMAIL.'>',
    'Reply-To'      => EACTIONS_DEBUG_EMAIL,
    'Return-Path'   => EACTIONS_DEBUG_EMAIL,
    );

    $body = "$from\n$toAddress\n$textMessage";

    return $this->mail($body, $headers);
    //        CRM_Core_Error::setCallback();
  }

}
