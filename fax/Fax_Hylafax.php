<?php
//TODO:
// CONVERT TO WORK WITH Fax.php
// - USE Fax_MyFax.php AS A MODEL

/**
* BE_Fax.class
*
* Fax utility for Back-End that works with the Hylafax fax engine
* (refer to Notes section for details)
*
* @package     lobby
* @author      Peter Bojanic
* @copyright   Copyright (C) 2003 OpenConcept Consulting
*
* This file is part of Back-End.
*
* Back-End is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* Back-End is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Back-End; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
* Notes:
* - THIS INITIAL IMPLEMENTATION WORKS ONLY WITH LINUX
* - this Fax class is designed to work with the Hylafax open source
*   fax server: http://www.hylafax.org/
* - the goal is for the class to become a more general-purpose fax
*   class that works with a variety of popular fax engines
* - the initial implementation doesn't do very much, but it simplifies
*   the job of sending faxes from php
*
* About Hylafax:
* - Red Hat RPMS may be obtained here:
*   ftp://ftp.hylafax.org/binary/linux/redhat/RPMS/i386
* - installation instructions are here: http://www.hylafax.org/setup.html
* - Back-End fax uses Hylafax's sendfax command, so there is no need
*   to set up the email-to-fax gatway unless you really want it
*/

class Fax_Hylafax extends Fax {

  function _writeMessageToFile($fileName, $message) {
    $success = false;
    if ($handle = fopen($fileName, "w")) {
      $success = fwrite($handle, $message);
      fclose($handle);
    }
    return $success;
  } //writeMessageToFile

  function _callFaxCommand($recipientFax, $fileName) {
    $command = "/usr/bin/sendfax -n -d $recipientFax $fileName";
    $result = exec($command);
    if ($result != "") {
    return true;
    }
    return false;
  } //callFaxCommand

  // send the text message to the specified rececipient fax number
  function send($recipientFax, $textMessage, $title='', $from = '', $to = '', $accountNumber = NULL) {
    $success = false;
    if ($fileName = tempnam("/tmp", "be_fax")) {
      if ($this->_writeMessageToFile($fileName, $textMessage)) {
        $newName = baseName($fileName).".txt";
        rename($fileName, $newName); // So Hylafax can use the extension
        $fileName = $newName;
        $success = $this->_callFaxCommand($recipientFax, $fileName);
      }
      unlink($fileName);
    }
    return $success;
  } //faxSend
} //BE_Hylaax

?>
