<?php
/**
* BE_Fax.class
*
* Fax utility for Back-End that works with the Hylafax fax engine
* (refer to Notes section for details)
*
* @package     lobby
* @author      Peter Bojanic
* @copyright   Copyright (C) 2003 OpenConcept Consulting
*
* This file is part of Back-End.
*
* Back-End is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* Back-End is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Back-End; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * Abstract class (interface) for fax modules
 *
 */
class Fax {

  /**
   * Return cleaned, validated fax number; return false if invalid
   *
   * Defaults to USA/Canada numbers for now
   *
   * @param String $faxNumber
   * @param Array $params Used for location-specific validation eg 'city', 'province', 'country', 'postal_code'
   * @return String/FALSE
   */
  function clean($faxNumber, $params=NULL) {
    $faxNumber = preg_replace('/[^0-9]/','',$faxNumber);

    if (strlen($faxNumber) != 10) {
      return FALSE;
    }

    // add default country code
    return "1".$faxNumber;
  }

  # special characters in the "to" field seem to throw errors
  function clean_to($to, $params=NULL) {
    # replace accented characters with unaccented version
    $to = unaccent($to);
    # remove non word, non digits
    # allow important characters (NB -- only tested for MyFax)
    $to = preg_replace('/[^\w\d\-\/\=\.]/','',$to);
    # remove multiple underscores
    $to = preg_replace('/_{2,}/','_', $to);
    return $to;
  }

  function unaccent($text) {
    static $search, $replace;
    if (!$search) {
      $search = $replace = array();
      // Get the HTML entities table into an array
      $trans = get_html_translation_table(HTML_ENTITIES);
      // Go through the entity mappings one-by-one
      foreach ($trans as $literal => $entity) {
        // Make sure we don't process any other characters
       // such as fractions, quotes etc:
        if (ord($literal) >= 192) {
          // Get the accented form of the letter
          // NB - get_html_translation_table produces ASCII-encoded literals
          $search[] = utf8_encode($literal);
          // Get e.g. 'E' from the string '&Eacute'
          $replace[] = $entity[1];
        }
      }
    }
    return str_replace($search, $replace, $text);
  }


  /**
   * Return cleaned, validated fax number; return false if invalid
   *
   * Currently, only North American (10 digit) codes are accepted
   *
   * @todo Location-specific validation - Maybe use http://www.webservicex.net/country.asmx?op=GetISD
   * @todo Deal with special codes like *70
   *
   * @param String $faxNumber
   * @param Array $params Used for location-specific validation eg 'city', 'province', 'country', 'postal_code'
   * @return String/FALSE
   */
  function clean($faxNumber, $params=NULL) {
    //    $faxNumber = ereg_replace("[^0-9]", "", $faxNumber); // only keep numbers
    $faxNumber = parent::clean($faxNumber);

    if (strlen($faxNumber) != 10) {
      return FALSE;
    }

    // add default country code
    return '1'.$faxNumber;
  }


  /**
   * Send the text message to the specified rececipient fax number
   *
   * @param string $toAddress Target's fax number
   * @param Striing $textMessage Body of email to send
   * @param String $title
   * @param String $from
   * @param String $to
   * @param String $billingCode
   * @return Boolean Success of sending email to MyFax
   */
  function send($recipientFax, $textMessage, $title='', $from = '', $to = '', $billingCode = '') {
    return false;
  } //faxTextMessage

} //BE_Fax

?>
