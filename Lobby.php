<?php
/**
 * Provides functionality to support a participant taking part in an EAction (ie lobby.module)
 *
 * In detail, this class provides functionality to support:
 * - Linking the current participant to the contact database (uses CiviCRM)
 * - Creating/updating contact details (uses CiviCRM)
 * - Recording the participation in this eAction/lobby {eaction_lobby2participant}
 * - Linking the participant to this lobby's targets {eaction_lobby2participant_target}
 * - Resolving target-types to contactable entities (BE_TaregtFinder*)
 * - Sending the message to the target(s) and recording the success/failure {eaction_lobby2participant_target}
 *
 * It also masks the details of data access from lobby.module eg by
 * handling the mapping between BE_Contact and CiviCRM field names
 * (BE_Contact fieldnames should be hidden from Drupal)
 *
 * Derived from BE_Action_participate
 *
 * @package     lobby
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

define('EACTIONS_FINDTARGETS_TIMELAG',5 * 24 * 60 * 60); // 5 days. Must be > 3600
define('EACTIONS_COOKIE_LIFETIME',  180 * 24 * 60 * 60); // 180 days

define('EACTIONS_TARGET_NOTFOUND',  0);
define('EACTIONS_TARGET_FOUND',     1);
define('EACTIONS_TARGET_FOUND_OLD', 2);
define('EACTIONS_TARGET_DELIVERED', 3);

// Data access classes
include_once 'Data/CRM.php';
include_once 'Data/Lobby.php';
include_once 'Data/Target.php';

class Lobby {

  /**
  * Id of node/lobby
  *
  * @var integer
  */
  var $nid;

  /**
  * @var Object
  */
  var $participant, $node;

  /**
  * @var Object CiviCRM contact
  */
  var $CRM;

  /**
   * Used to flag if participant was already known, or had to be guessed/created
   *  - used when working out if it's best to create a new contact. May not be necessary
   * @var Boolean
   * @access private
   */
  var $_direct_info;

  /**
   * Used to flag whether participant's activities should be logged
   * @var Boolean
   * @access private
   */
  var $_logging;

  /**
   * Used to flag whether participant should be added to the Lobby's group
   * @var Boolean
   * @access private
   */
  var $_opt_in;


  /**
  * Constructor
  *
  * @param node The current node object
  */

  function Lobby($node) {
    $this->node     = $node;
    $this->nid      = $node->nid;
    $this->_logging = !(FALSE == variable_get('lobby_logging',FALSE));
  }

  /**
  * Fetches participant info associated with the given Drupal user
  * @todo Make use of 'Remember Me' info if not logged in user
  *
  * @param  Type Desc
  * @return array
  * @access public
  */


  /**
  * Work out what information can be found on this participant
  *
  * Maybe we should leave all the work to CiviCRM - but at the moment
  * $user and $_COOKIE are examined here for clues to the participant's
  * identity
  *
  * (OLD) From BE:
  * Fill in a DO_Be_participant object according to either the
  * author information, the participant_info cookie, or the $ary (GET/POST)
  * fields. If the new participant is very similar, update and reuse the
  * table row; otherwise unset the key, so a new participant will be created,
  * but set a field which will link the new participant instance
  * to the previous one.
  * Rationale: A signature in a petition should not change, so new instance
  * author should take priority over cookie if both exist, to better allow
  * multi-user/single-machine scenarios
  *
  * @param Array Values for search key if participant can't be identified from Drupal login or cookie
  * @return Array Participant field values
  */
  function getParticipant($values=NULL) {

    $this->participant = NULL;
    $lookup = $this->_participantFromEnvironment();

    if ($lookup) {
      $this->_direct_info = TRUE;
    }
    else {
      $this->_direct_info = FALSE;
      if (isset($values['email'])) {
        $lookup['email'] = $values['email'];
        if (isset($values['last_name'])) {
          $lookup['last_name'] = $values['last_name'];
        }
      }
    }

    if (!$lookup) {
      return FALSE;
    }

    // We've gone *some* info in $lookup, let's see if CiviCRM can work out who we've got

    load_crm_contact();
    $this->CRM = &crm_get_contact($lookup); //, array('contact_id','display_name','first_name','last_name','postal_code','email','random_key'));
    if (is_a($this->CRM, 'CRM_Core_Error')) {
      return FALSE;
    }

    $p = Data_CRM::useful_fields($this->CRM);
    if (!empty($lookup['email']) && $p->email != $lookup['email']) {
      watchdog('lobby',sprintf('Looking for %s, CRM returned %s', $lookup['email'], $p->email));
      return FALSE;
    }

    $this->participant = $p;

    return $this->_toArray($this->participant);
  }



  // Helper functions for getParticipant

  /**
  * Find and store any information that is available for identifying the participant
  *
  * @todo Use cookie info for RememberMe functionality
  *
  * @return Array Search key to use for lookup up particiapent
  * @access private
  */
  function _participantFromEnvironment() {
    global $user;

    $lookup = NULL;

    // Use currently logged in user if possible
    if ($user->uid != 0) {
      $contactID = Data_CRM::contact_id($user->uid);
      if ($contactID) {
        // User has matchng CRM record
        $lookup['contact_id'] = $contactID;
      }
      elseif(!empty($user->mail)) {
        // Else, try for implicit link using email
        $lookup['email'] = $user->mail;
      }
    }

    // Not logged in - there may be a cookie, though
    //    elseif (!empty($_COOKIE['participant_info'])) {
    //      $lookup['random_key'] = $_COOKIE['participant_info'];
    //    }

    //    require_once 'CRM/Core/BAO/UFMatch.php';
    //    CRM_Core_BAO_UFMatch::synchronize( $user, false, 'Drupal' );

    return $lookup;
  }


  //  /**
  //  * Create or reuse a random value for the participant.
  //  * (do not use the ID for security reasons)
  //  * Set it as the cookie.
  //  * @todo Save random key in CiviCRM record
  //  * @access private
  //  */
  //  function _setParticipantCookie() {
  //    $randomKey = $this->participant->random_key;
  //    if (!$randomKey) {
  //      $randomKey = generateRandomKey(8);
  //      $this->participant->random_key = $randomKey;
  //      if (isset($this->CRM)) {
  //        load_crm_contact();
  //        crm_update_contact($this->CRM,array('random_key'=>$randomKey));
  //      } else {
  //        $this->participant->random_key = $randomKey;
  //      }
  //    }
  //
  //    setcookie('participant_info', $randomKey, time()+EACTIONS_COOKIE_LIFETIME, '/', str_replace('www.','', $_SERVER['SERVER_NAME']));
  //  }
  //
  //  /**
  //  * @see _setParticipantCookie()
  //  * @access private
  //  */
  //  function _clearParticipantCookie() {
  //    setcookie('participant_info', "", 0, '/', str_replace('www.','.', $_SERVER['SERVER_NAME']));
  //    unset($this->participant->random_key);
  //    if (isset($this->CRM)) {
  //      // Belt and braces - not sure this is necessary
  //      crm_update_contact($this->CRM,array('random_key'=>NULL));
  //    }
  //  }


  /**
  * Update participant record with confirmed values
  *
  * A new participant is created if one cant be found by @see(getParticipant) or the surname has changed
  *
  * @todo Restore BE funcitonality whereby 'SameContactAs' can be used to backgrack changes in participant's details
  * @todo Add error condition/activity history record where match is found, but participants surname doesn't match (or some other check that the participant is not similar enough to the existing contact)
  *
  * @param  Array The known values
  * @access $public
  */
  function updateParticipant($values) {

    // Clear out blank fields to prevent accidental overwriting
    foreach ($values as $k => $v) {
      if (empty($v)) {
        unset($values[$k]);
      }
    }

    $known = $this->getParticipant($values);

    // Update if the contact is known, or similar enough to allow an update
    // otherwise create a new contact
    if ($known && ($this->_direct_info || strcasecmp($values['last_name'],$this->participant->last_name) == 0)) {
      $action = '_updateParticipant';
    }
    else {
      $action = '_createParticipant';
    }
    $contact_id = $this->$action($values);
    if (!$contact_id) {
      return FALSE;
    }
    $this->_opt_in = $values['opt_in'] ? TRUE : FALSE;

    assert('!empty($this->participant->contact_id)');
    return $contact_id;
  }


  /**
  * Pull out the participant's previous lobbying attempt (if any)
  * @return Array/NULL
  */
  function fetchLobby() {
    $link = Data_Lobby::load_participant_lobby($this->nid, $this->participant->contact_id);
    if (!$link) {
      return NULL;
    }
    return $link;
  }

  /**
  * Links the current participant to this Lobby and save their comment
  *
  * @return Boolean
  */
  function linkToLobby($comment) {
    //    $values['created'] = time();
    if (!Data_Lobby::create_participant_lobby(array($this->nid, $this->participant->contact_id),array('comment' => $comment))) {
      return FALSE;
    }
    //    $this->comment
    return TRUE;
  }


  /**
   * Basic info on direct targets and target types
   * - targets info associated with this target-type is shown if already available
   * @see getAndResolveTargets()
   * @return array Of target objects
   */
  function getExisingTargets() {

    $targets     = $this->_listDirectTargets();

    $targetTypes = $this->_listTargetTypes();
    if ($targetTypes) {

      $nid = $this->nid;
      $participantId = $this->participant->contact_id;

      // Get uptodate list of the targets we've found for this participant
      // - key by target_type_id for later
      // TODO - write this so that the info is all pulled off the DB in one query
      $participantTargets = NULL;
      if ($participantId) {
        $linkList = Data_Lobby::load_participant_targets(array($nid, $participantId));
        if (is_array($linkList)) {
          foreach ($linkList as $v) {
            if ($v->ttid != 0) {
              $participantTargets[$v->ttid][] = $v;
            }
          }
        }
      }

      // Here, we fetch the generated/matched targets...
      foreach ($targetTypes as $ttid => $targetType) {
        // Check list of linked targets for one of this type
        $tid = $participantTargets[$ttid][0]->tid;
        if (empty($tid)) {
          $targets[$targetType]->display_name = $targetType;
        }
        elseif ('4' == $targetType) {// **** Hack for Data_Candidate til I come up with something better
          $targets = $targets + Data_Candidate::list_all($this->participent->edid);
        }
        else {
          $targets[$targetType] = Data_Target::load($tid);
        }
      } // end foreach

    }
    return $targets;
  }


  /**
  * Find and update info on direct targets and target types
  *
  * New target records will be created if necessary
  * and links are also built between the participants and targets (direct and indirect)
  *
  * Saved search results are used, which is why it makes sense for this method to combine
  * target identification and linking between participant and target
  *
  * @see getExistingTargets() which is used if the data should not be changed
  *
  * @return array Of target objects
  */
  function getAndResolveTargets() {
    assert('is_object($this->participant)');

    $now = time();

    // Link the participant to the direct targets for this lobby
    $targets     = $this->_listDirectTargets();
    if (is_array($targets)) {
      foreach ($targets as $v) {
        $this->_saveTargetLink(0, $v->tid, EACTIONS_TARGET_FOUND, $now);
      }
    }

    // We can assume that the participant has not taken part in this lobby
    //assert('!Data_lobby::load_participant_targets(array($this->nid, $this->participant->contact_id))');

    $lobbyTargetTypes = Data_Lobby::load_target_types($this->nid);
    if ($lobbyTargetTypes) {
      // There are some targettypes to resolve
      // - and they won't have been resolved yet else we wouldnt have got this far in the code

      $targetTypes    = $this->_listTargetTypes($lobbyTargetTypes);
      $BEParticipant  = $this->_makeBEParticipant();

      foreach ($targetTypes as $ttid => $targetType) {
        // Find and resolve the target types (and link them to the participant)

        $targetFinderClass = 'BE_TargetFinder'.$targetType;
        include_once "targetfinder/$targetFinderClass.class";
        /* @var $targetFinderObj BE_TargetFinder */
        $targetFinderObj = new $targetFinderClass;
        $target = $targetFinderObj->identifyTarget($BEParticipant);

        if (!$target) {
          // Target not found. Dont try looking again for a while
          // - NB May already be linked if this generated target is also a direct target
          $this->_saveTargetLink($ttid, NULL, EACTIONS_TARGET_NOTFOUND, $now);
          $targets[$targetType]->display_name = $targetType;
          watchdog('lobby',sprintf('Couldnt resolve targettype %s for participant %s %s (%d)', $targetType, $this->participant->email, strtoupper(str_replace(" ",'',$this->participant->postalCode)), $this->participant->contact_id));
          continue;
        }

        // Target(s) resolved for this target type
        // - build a link between target and participant
        // - NB May already be linked if this generated target is also a direct target
        if (is_array($target)) {
          foreach ($target as $t) {
//            $t = Data_Target::crm_fields($t_be);
            $targets[$t->tid] = $t;
            $this->_saveTargetLink($ttid, $t->tid, EACTIONS_TARGET_FOUND, $now);
          }
        }
        else {
          //            assert('!empty($target->displayName)');
          $t = Data_Target::crm_fields($target);
          $targets[$t->tid] = $t;
          $this->_saveTargetLink($ttid, $t->tid, EACTIONS_TARGET_FOUND, $now);
        }

      }

    }

    /*
    // Here, we set the generated targets...
    foreach ($targetTypes as $ttid => $targetType) {

    // Check list of linked targets for one of this type
    $tid = $participantTargets[$ttid][0]->tid;

    if ($tid && ($now - $participantTargets[$ttid][0]->last_checked <= EACTIONS_FINDTARGETS_TIMELAG)) {
    // This participant has been busy - we've found an existing uptodate target for this type
    $target = Data_Target::load($tid);
    $targets[$targetType] = $target;
    }

    else {

    // There is no uptodate link to a target of the required type for this participant
    // So, resolve the target
    $targetFinderClass = 'BE_TargetFinder'.$targetType;
    include_once "targetfinder/$targetFinderClass.class";
    /* @var $targetFinderObj BE_XPathTargetFinder *
    $targetFinderObj = new $targetFinderClass;
    $target = $targetFinderObj->identifyTarget($BEParticipant);

    if ($target) {
    //            assert('!empty($target->displayName)');
    // Target found
    // - build a link between target and participant
    // - NB May already be linked if this generated target is also a direct target
    $targets[$targetType] = Data_Target::crm_fields($target);
    $tid = $targets[$targetType]->tid;
    $this->_saveTargetLink($ttid, $tid, EACTIONS_TARGET_FOUND, $now);
    }

    elseif ($tid) {
    // Didn't find a current target, so use the old one. why not.
    // - build a link between target and participant
    // - and flag so that the link's not rechecked for an hour
    // - NB May already be linked if this generated target is also a direct target
    $targets[$targetType] = Data_Target::load($tid);
    $this->_saveTargetLink($ttid, $tid, EACTIONS_TARGET_FOUND_OLD, $now-EACTIONS_FINDTARGETS_TIMELAG+3600);
    watchdog('lobby',sprintf('Using old target for targettype %s for participant %s - %s (%d)', $targetType, strtoupper(str_replace(" ",'',$this->participant->postalCode)), $this->participant->email, $this->participant->contact_id));
    }

    else {
    // Target not found. Dont try looking again for a while
    // - NB May already be linked if this generated target is also a direct target
    $this->_saveTargetLink($ttid, NULL, EACTIONS_TARGET_NOTFOUND, $now);
    $targets[$targetType]->display_name = $targetType;
    watchdog('lobby',sprintf('Couldnt resolve targettype %s for participant %s -%s (%d)', $targetType, $this->participant->email, strtoupper(str_replace(" ",'',$this->participant->postalCode)), $this->participant->contact_id));
    }

    }

    } // end foreach
    */

    return $targets;
  }

  /**
   * Create or update link betwen participant, target and lobby
   *
   * The aim is to prevent duplicate links arising for a target (eg because the target is direct and would also be generated)
   * and to ensure that there is only one record for the target-type
   *
   * ALTERNATIVE APPROACH WHEN TARGET IS BOTH DIRECT AND RESOLVED
   * Could create a separate record for each, but flag the second etc records as being duplicates??
   *
   * Permutations
   * set(tid) && empty(ttid)   Create/update record for this direct target
   * set(tid) && set(ttid)     Create/update record for this resolved target
   * empty(tid) && set(ttid)   Create/update dummy record for this target type
   * empty(tid) && empty(ttid) Error
   *
   * @todo It should be possible to not have to call Data_Lobby::load_participant_target since all the info is around anyway
   *
   *
   * @param Integer $ttid
   * @param Integer $tid
   * @param Integer $status
   * @param Integer $time
   * @return Boolean
   * @access private
   */
  function _saveTargetLink($ttid, $tid, $status=NULL, $time=NULL) {
    $values = NULL;
    if (isset($status)) {
      $values['status'] = $status;
    }
    if (isset($time)) {
      $values['last_checked'] = $time;
    }
    $nid = $this->nid;
    $participantId = $this->participant->contact_id;
    assert('!empty($participantId)');

    // If the target is known, create or update a link record
    if (!empty($tid)) {
      $oldLink =  Data_Lobby::load_participant_target(array($nid, $participantId), array('tid' => $tid));

      if (!empty($oldlink->ttid)) {
        // **** Need to say load in where $ttid in 0..3 (BE_Contacts) xor 4 (= CA Candidates)
        $oldLink = ($ttid == $oldLink->ttid) || in_array($oldLink->ttid, array(0,1,2,3)); // *** UGH!!
        // $oldLink is now a Boolean. Isn't PHP great!
      }
      if (!empty($oldlink->ttid)) {
        Data_Lobby::update_participant_target(array($nid, $participantId, $tid),$values);
        return TRUE;
      }
      else {
        Data_Lobby::insert_participant_target(array($nid, $participantId, $tid, $ttid),$values);
        return TRUE;
      }
    }

    // Target is not yet known (or not available to this participant - eg wrong province).
    // So, check for an existing link for this target type
    elseif (!empty($ttid)) {
      // This is assuming that there isnt already a ttid record with a value for tid
      $oldLink =  Data_Lobby::load_participant_target(array($nid, $participantId), array('ttid' => $ttid));
      if ($oldLink) {
        Data_Lobby::update_participant_target(array($nid, $participantId, $oldLink->tid, $ttid),$values);
        return TRUE;
      }
      else {
        Data_Lobby::insert_participant_target(array($nid, $participantId, 0, $ttid),$values);
        return TRUE;
      }
    }

    // Error: Not enough information
    return FALSE;
  }


  /**
  * Saves/updates link from participant to target(s) for the EAction
  * - saves the participant's comments too
  * @todo Be cleverer about the cirumstances where a direct target is the same as a calculated one
  *
  * @param  Array/Object? Details of targets to link to
  * @param Array Containing comments
  * @access private
  */

  //  function _linkToTarget($targets, $values) {
  //    foreach ($targets as $k => $v) {
  //      Data_Lobby::update_participant_target(array($this->nid,$this->participant->contact_id,$v['ttid'],$v['tid']),array('comment' => $values['comment']));
  //      // OR, if a new link:
  //      Data_Lobby::insert_participant_target(array($nid, $participantId, $targetType, $tid),
  //      array('status' => 1,'last_checked' => time() - EACTION_FINDTARGETS_TIMELAG + 3600));
  //    }
  //  }
  //

  /**
  * Cycle through all targets and attempt to send messages to them, recording successes and failures
  *
  * This code allows for participant resending comments if it failed the first time
  *
  * Based on do_send(&$action, &$data, &$errors)
  * @todo Work out and reproduce what the Kiosk-Mode code does
  * @param  Array List of targets
  * @return Boolean
  * @access public
  */

  function sendToTargets($targets) {
    if (!is_array($targets)) {
      return FALSE;
    }
    assert('!empty($this->nid)');
    assert('is_object($this->participant) && !empty($this->participant->contact_id)');
    $nid           = $this->nid;
    $participantId = $this->participant->contact_id;
    $isOk          = TRUE;

    // Pull in details (eg for comments etc)
    // NB this data is probably already around.
    $participantLobby = Data_Lobby::load_participant_lobby($nid, $participantId);
    // List of past attempts at contact, keyed by target id
    $participantTargets = Data_Lobby::load_participant_targets(array($nid, $participantId));

    // targetLinks gets a list of all resolved targets, indexed by target id
    $targetLinks = NULL;
    foreach ($participantTargets as $v) {
      if ($v->tid != 0) $targetLinks[$v->tid] = $v;
    }

    // Run through each participant/action record
    foreach ($targets as $target) {
      if (empty($target->tid)) {
        // Unresolved target, so no message can be sent
        continue;
      }
      $targetLink = $targetLinks[$target->tid];
      if ($targetLink->status == EACTIONS_TARGET_DELIVERED) {
        // This target has already been successfully contacted (shouldnt happen)
        // TODO: could raise error message, or watchdog it
        continue;
      }

      // OK, we can now send a message
      if ($this->_sendOneMessage($participantLobby, $target, $targetLink)) {
        if (!Data_Lobby::update_participant_target(array($nid, $participantId, $targetLink->tid, $targetLink->ttid),
        array('last_checked'=>time(),'status'=>EACTIONS_TARGET_DELIVERED))) {
          watchdog('lobby',sprintf('Lobby %d: After sending message, could not update link between participant (%d) and target (%d/%d)',$nid, $participantId, $targetLink->tid, $targetLink->ttid),WATCHDOG_ERROR);
        }
      }
      else {
        $isOk = FALSE;
      }

    }

    Data_Lobby::increment_access_count($nid); // Doesnt work consistently - due to lack of locking on table??

    // Finally, add the participant to the Lobby's group
    assert('is_object($this->CRM)');
    if ($this->_opt_in) {
      $groups = crm_get_groups(array('name'=>'Lobby'.$nid));
      if (count($groups) != 1) {
        watchdog('lobby',sprintf('Linking partipant to Lobby, did not find the single expected CRM group with name=Lobby%d',$nid),WATCHDOG_ERROR);
        return;
      }
      $contacts = array($this->CRM);
      $error = crm_add_group_contacts($groups[0], $contacts, 'Added', 'API');
      if ($error) {
        watchdog('lobby',sprintf('Linking partipant to Lobby, could not add %s to CRM group with name=Lobby%d',$this->participant->email, $nid),WATCHDOG_ERROR);
      }
    }

    return $isOk;

  }


  /**
 * Send a message to a target
 *
 * @todo Workout participant's address information to show on message
 *
 * @param Object $participantLobby record (Participant's own contribution)
 * @param Object $target record for name and contact details of the target
 * @param Object $targetLink record For updating status?? (not used at the mo)
 * @return Boolean
 */
  function _sendOneMessage($participantLobby, $target) {
    static $send;
    if (!isset($send)) {
      switch ($this->node->method) {
        case 1:
        $send = '_sendEmail';
        break;

        case 2:
        $send = '_sendFax';
        break;

        default:
        return FALSE;
      }
    }
    $ary = array('from_name' => $this->participant->display_name,
    'from_email' => $this->participant->email,
    'to_name'   => empty($target->display_name) ? $target->first_name.' '.$target->last_name : $target->display_name,
    'to_email'  => $target->email,
    'to_fax'    => $target->fax,
    'title'     => $this->node->title,
    'message'   => $this->node->message,
    'comment'   => $participantLobby->comment,
    'today'     => format_date(time(),'large'),
    'files'     => $this->node->files,
    );

    if (empty($target->salutation)) {
      $ary['salutation'] = t('Dear Sir/Madam');
    }
    else {
      $ary['salutation'] = t('Dear %salut',array('%salut'=>$target->salutation));
    }
    // don't send anything if we're testing
    if (LOBBY_TESTING===FALSE) {
      return $this->$send($ary);
    }
    else {
      return TRUE;
    }

  } //sendOneMessage


  /**
  * @todo Convert to use email template?
  * @access private
  * @return Boolean
  */
  function _sendEmail($ary) {

    $nid = $this->nid;
    if (empty($ary['to_email'])) {
      watchdog('lobby',sprintf('Couldn\'t send email to target %s because the target had no email address', $ary['to_name']));
      return FALSE;
    }

    $body = <<<EOT
Date: $ary[today]

From:
$ary[from_name]
$ary[from_address]
Email: $ary[from_email]

To:
$ary[to_name]
$ary[to_address]
Email: $ary[to_email]

Subject: $ary[title]

$ary[salutation]

$ary[message]

$ary[comment]

$ary[from_name]
EOT;


    $headers = array(
    'Subject'       => $ary['title'],
    'From'              => $ary['from_email']
    //    'From'          => '"'. $ary['from_name'] .'"' . ' <' . $ary["from_email"] . '>'
    //    'Reply-To'      => $ary[from_name] . ' <' . $ary["from_email"] . '>',
    //    'Return-Path'   => $ary[from_name] . ' <' . $ary["from_email"] . '>',
    );

    if (defined('EACTIONS_DEBUG_EMAIL')) {
      $headers['To'] = EACTIONS_DEBUG_EMAIL;
      $body = $ary['to_email'] ."\n". $body;
    }
    else {
      $headers['To'] = $ary['to_email'];
    }

    $success = eactions_mail($body, $headers, @$this->node->files);

    if (!$success) {
      // TODO: need to sort out callback for detail display -- Will have to sort out the detailed display later MG
      $params = array(
      'activity_id' => $nid,
      'entity_table' => 'civicrm_contact',
      'activity_type' => 'Lobby',
      'module' => 'Lobby',
      # 'callback' => 'LobbyActivity::display',
      'activity_summary' => t('Failed to send email to target %target (%to)', array('%target' => $ary['to_name'], '%to' => $ary['to_email'])),
      'activity_date' => date('YmdHis'),
      'entity_id' => $this->participant->contact_id,
      );
      $historyItem =& crm_create_activity_history($params);
      watchdog('lobby',sprintf('Couldn\'t send email to target %s (%s) for participant %s (%s)', $ary['to_name'], $ary['to_email'], $ary['from_name'], $ary['from_email']), WATCHDOG_WARNING);
      return FALSE;
    }

    if ($this->_logging) {
      // TODO: need to sort out callback for detail display -- Will have to sort out the detailed display later MG
      $params = array(
      'activity_id' => $nid,
      'entity_table' => 'civicrm_contact',
      'activity_type' => 'Lobby',
      'module' => 'Lobby',
      # 'callback' => 'LobbyActivity::display',
      'activity_summary' => t('Sent email to target %target (%to)', array('%target' => $ary['to_name'], '%to' => $ary['to_email'])),
      'activity_date' => date('YmdHis'),
      'entity_id' => $this->participant->contact_id,
      );
      $historyItem =& crm_create_activity_history($params);
      //      watchdog('lobby',sprintf('Participant %s (%s) sent email to target %s (%s)', $ary['from_name'], $ary['from_email'], $ary['to_name'], $ary['to_email']));
    }
    return TRUE;
  }

  /**
  * @todo Adjust for Drupal
  * @access private
  * @return Boolean
  */
  function _sendFax($ary) {

    $nid = $this->nid;

    if (defined('EACTIONS_DEBUG_FAX')) {
      $toFax = EACTIONS_DEBUG_FAX; //$target->faxNumber;
    }
    else {
      $toFax = $ary['to_fax'];
    }
    if (!$toFax) {
      watchdog('lobby',sprintf('Couldn\'t send fax to target %s - no fax number supplied', $ary['to_name']), WATCHDOG_WARNING);
      return FALSE;
    }

    $body = <<<EOT
$ary[salutation]

$ary[message]

$ary[comment]

$ary[from_name]
EOT;

    $faxClass = 'Fax_'.EACTIONS_FAX_MODULE;
    require_once "fax/$faxClass.php";
    /* @var $fax Fax */
    $fax = new $faxClass();

    $toFax = $fax->clean($toFax);
    if (!$toFax) {
      watchdog('lobby',sprintf('The fax number of Target %s (%s) is not valid', $ary['to_name'], $ary['to_fax']), WATCHDOG_WARNING);
      return FALSE;
    }

    $ary['billing_code'] = EACTIONS_FAX_BILLINGCODE;
    if (EACTIONS_FAX_BILLINGCODEUSEACTIONNO === TRUE) {
      $ary['billing_code'] .= '_'.$this->nid; //$actionID;
    }

    $success = $fax->send($toFax, $body, $ary);
    if (!$success) {
      // TODO: need to sort out callback for detail display -- Will have to sort out the detailed display later MG
      $params = array(
      'activity_id' => $nid,
      'entity_table' => 'civicrm_contact',
      'activity_type' => 'Lobby',
      'module' => 'Lobby',
      # 'callback' => 'LobbyActivity::display',
      'activity_summary' => t('Failed to send fax to target %target (%to)', array('%target' => $ary['to_name'], '%to' => $ary['to_email'])),
      'activity_date' => date('YmdHis'),
      'entity_id' => $this->participant->contact_id,
      );
      $historyItem =& crm_create_activity_history($params);
      watchdog('lobby',sprintf('Couldn\'t send fax to target %s (%s) for participant %s (%s)', $ary['to_name'], $ary['to_fax'], $ary['from_name'], $ary['from_email']), WATCHDOG_WARNING);
      return FALSE;
    }

    if ($this->_logging) {
      // TODO: need to sort out callback for detail display -- Will have to sort out the detailed display later MG
      $params = array(
      'activity_id' => $nid,
      'entity_table' => 'civicrm_contact',
      'activity_type' => 'Lobby',
      'module' => 'Lobby',
      # 'callback' => 'LobbyActivity::display',
      'activity_summary' => t('Sent fax to target %target (%to)', array('%target' => $ary['to_name'], '%to' => $ary['to_email'])),
      'activity_date' => date('YmdHis'),
      'entity_id' => $this->participant->contact_id,
      );
      $historyItem =& crm_create_activity_history($params);
      //      watchdog('lobby',sprintf('Participant %s (%s) sent fax to target %s (%s)', $ary['from_name'], $ary['from_email'], $ary['to_name'], $ary['to_fax']));
    }
    return TRUE;
  }


  /**
  * Put together a legacy participant object with enough information for old BE code
  * @access private
  * @return object
  */
  function _makeBEParticipant() {
    $out->contactID   = $this->participant->contact_id;
    $out->postalCode  = $this->participant->postal_code;
    $out->displayName = $this->participant->display_name ? $this->participant->display_name : $this->participant->title.' '.$this->participant->first_name.' '.$this->participant->last_name;
    $out->email       = $this->participant->email;
    $out->faxNumber   = @$this->participant->fax;
    return $out;
  }

  /**
  * Use array and existing target info
  * @todo CiviCRM linkup
  * @access private
  */
  function _updateParticipant($values) {
    load_crm_contact();
    $CRM = crm_update_contact($this->CRM, $values);
    if (is_a($CRM, 'CRM_Core_Error')) {
      watchdog('lobby',sprintf('Could not create update contact %s %s with email address=%s. Error message was: %s', $values['first_name'],$values['last_name'],$values['email'], $this->CRM->_errors[0]['message']),WATCHDOG_ERROR);
      return FALSE;
    }
    $this->CRM = $CRM; // Only overwrite if save went OK
    $this->participant = Data_CRM::useful_fields($this->CRM);
    return $this->CRM->contact_id;
  }

  /**
  * Use array and existing target info
  * @access private
  */
  function _createParticipant($values) {
    load_crm_contact();
    $values['random_key'] = time();
    $this->CRM = crm_create_contact($values);
    if (is_a($this->CRM, 'CRM_Core_Error')) {
      watchdog('lobby',sprintf('Could not create create contact %s %s with email address=%s. Error message was: %s', $values['first_name'],$values['last_name'],$values['email'], $this->CRM->_errors[0]['message']),WATCHDOG_ERROR);
      unset($this->CRM);
      return FALSE;
    }
    $this->participant = Data_CRM::useful_fields($this->CRM);
    return $this->CRM->contact_id;
  }


  /**
  * Return list of all identified targets for this lobby
  * @access private
  * @return array
  */
  function _listDirectTargets() {
    $fullTargets = Data_Lobby::load_direct_targets($this->nid);
    return $fullTargets;
  }

  /**
  * Return details of the valid target types for this lobbying action,
  * arranged to it's easy to check them against eaction_contact2target
  * @todo Pull in real list of target types
  * @access private
  * @return array
  */
  function _listTargetTypes($ttids = NULL) {
    if (!isset($ttids)) {
      $ttids = Data_Lobby::load_target_types($this->nid);
    }
    $lobby_target_list = variable_get('lobby_target_list',FALSE);

    if (is_array($ttids)) {
      $out = NULL;
      foreach ($ttids as $v) {
        $out[$v] = $lobby_target_list[$v];
      }
    } else {
      $out = array();
    }

    return $out;
  }

  /**
  * Maps an object to an associative array, stripping out non-scalars
  * @access private
  * @return array
  */
  function _toArray($obj) {
    $out = NULL;
    if (is_object($obj)) {
      foreach ($obj as $k => $v) {
        if (is_scalar($v)) {
          $out[$k] = $v;
        }
      }
    }
    return $out;
  }

}
?>
