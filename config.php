<?php
/**
 * General config, including info used by the Back-End originated code for finding files to include
 *
 * @package     lobby
 * @copyright   Copyright (C) 2005 OpenConcept Consulting
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

define('EACTIONS_ROOT','/var/www/drupal/modules/lobby');
define('EACTIONS_LOBBY_VERSION','1.0');

// set to TRUE for development work; no messages are sent
define('LOBBY_TESTING', FALSE);

// FAX MODULE SETTINGS
// Comment these lines out if you do not require fax functionality

define('EACTIONS_FAX_MODULE',                 'Debug'); // Debug, MyFax or Hylafax
define('EACTIONS_FAX_SENDER',                 'debug@example.com');
define('EACTIONS_FAX_BILLINGCODE',            'Lobby');

// If TRUE, the Lobby's node-id is added to BILLINGCODE when the Fax is generated
define('EACTIONS_FAX_BILLINGCODEUSEACTIONNO', TRUE);

// define('EACTIONS_FAX_PASSWORD',               '******');

// Only try with TRUE once you know things are working
define('EACTIONS_FAX_SPACEALLOWED', FALSE);

// Show fax/email for targets when participating in a campaign
define('EACTIONS_SHOW_CONTACT_DATA', FALSE);

// Hide/Display/Modify Text for signon page
// define('EACTIONS_SHOW_CONTACT_DATA', FALSE);
// define('EACTIONS_HIDE_TITLE', FALSE);
// define('EACTIONS_HIDE_CITY', FALSE);
// define('EACTIONS_HIDE_PROVINCE', FALSE);
// define('EACTIONS_HIDE_COUNTRY', FALSE);
// define('EACTIONS_HIDE_POSTALCODE', FALSE);
// define('EACTIONS_POSTALCODE_TITLE', 'Postal Code');
// define('EACTIONS_POSTALCODE_DESC', 'Used for target generation');
// define('EACTIONS_OPTIN_TITLE', 'Sign me up for more information.');
// define('EACTIONS_OPTIN_DESC', 'Get our updates');
// define('PROVINCE_TITLE', 'Province');
// define('EACTIONS_LETTER_HEADER', 'Your letter');
// define('EACTIONS_CORRECTIONS', 'Make corrections');
// define('EACTIONS_ACCEPT_LINK)', 'Accept');


// DEBUG SETTINGS - Comment these out when not in debug mode
// define('EACTIONS_DEBUG_EMAIL','debug@example.com');
// define('EACTIONS_DEBUG_FAX',  '111 222 5555');

// Activate assertion - for debugging
assert_options(ASSERT_ACTIVE,  1);
assert_options(ASSERT_WARNING, 1);
assert_options(ASSERT_BAIL,    1);


// INTERNAL CONFIGURATION

// Currently, there are two places to look for PEAR modules - under eactions and civicrm
// - we need to have one place to find the modules
/*$include_path = '.' . PATH_SEPARATOR . EACTIONS_ROOT . PATH_SEPARATOR
. get_include_path() . PATH_SEPARATOR . EACTIONS_ROOT.DIRECTORY_SEPARATOR.'pear';
set_include_path($include_path);
*/
$include_path = PATH_SEPARATOR . EACTIONS_ROOT . PATH_SEPARATOR . EACTIONS_ROOT.DIRECTORY_SEPARATOR.'pear';
set_include_path(get_include_path() . $include_path);
// Legacy for findtarget: Just in case set_globals is on
global $_BE;
$BE = NULL;
?>
