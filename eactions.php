<?php
/*
* @file
* General Functions used by the EActions code
*
* @todo Possibly split between String and Messaging modules
*
* @package     lobby
* @copyright   Copyright (C) 2005 OpenConcept Consulting
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * Returns a Random value for URL/user identification.  One of the
 * very few functions lifted wholesale from the original PET_Petition
 * implementation.
 *
 * @param int      $length The length of the value - the default is 8 characters.
 * @return string          The generated random string.
 */

function generateRandomKey($length = 8) {
  mt_srand((double)microtime() * 1000000);
  $possible = '0123456789abcdefghjiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $str = '';
  while (strlen($str) < $length) {
    //      $str .= substr($possible, mt_rand(0, strlen($possible) - 1), 1);
    $str .= $possible{mt_rand(0, strlen($possible) - 1)};
  }
  return $str;
}


// DRUPAL-STYLE FUNCTIONS ============================================

/**
 * Check whether given string is a valid postal code. An empty code is considered invalid
 *
 * I would have thought this functionality would be provided by Drupal or CiviCRM
 *
 * Regexs mostly come from http://www.regexlib.com/
 *
 * @param string $code
 * @param integer $country_id Per crm_country table
 * @return Boolean
 */

function valid_postal_code($code, $country=1039) {
  $code = trim($code);
  if (empty($code)) {
    return FALSE;
  }
  if (is_array($country)) {
    foreach ($country as $v) {
      if (valid_postal_code($code,$v)) {
        return TRUE;
      }
    }
    return FALSE;
  }
  switch ($country) {
    case 1039: // Canada
    $code = preg_replace("/[\s|-]/", '', $code);
    return (preg_match('/([ABCEGHJKLMNPRSTVXY]\d[A-Z]\s?\d[A-Z]\d)/i', $code));

    case 1228: // USA
    return preg_match('/(\d{5}[ -]\d{4})|(\d{5})/', $code);

    case 1226: // UK
    return preg_match('/(((^[BEGLMNS][1-9]\d?)|(^W[2-9])|(^(A[BL]|B[ABDHLNRST]|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]|F[KY]|G[LUY]|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]|M[EKL]|N[EGNPRW]|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKL-PRSTWY]|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)\d\d?)|(^W1[A-HJKSTUW0-9])|(((^WC[1-2])|(^EC[1-4])|(^SW1))[ABEHMNPRVWXY]))(\s*)?([0-9][ABD-HJLNP-UW-Z]{2}))$|(^GIR\s?0AA$)/i',$code);

    default:
    return TRUE;
  }
}
// This alternative approach uses CIVICRM settings to work out which countries are valid
// Not used for now since it requires the whole module to be loaded before the country codes can be worked out
// - seems very heavyweight
//function valid_postal_code($code) {
//  $code = trim($code);
//  if (empty($code)) {
//    return FALSE;
//  }
//  // limit the state/province list to the countries specified in CIVICRM_PROVINCE_LIMIT
//  $config =& CRM_Core_Config::singleton();
//  $countryIsoCodes = CRM_Core_PseudoConstant::countryIsoCode();
//  $limitCodes = preg_split('/[^a-zA-Z]/', $config->provinceLimit);
//  $limitIds = array();
//  foreach ($limitCodes as $country) {
//    switch ($country) {
//      case 'CA':
//      // Simple check for a Canadian postcode (could be improved)
//      $code = preg_replace("/[\s|-]/", '', $code);
//      if (preg_match('/([ABCEGHJKLMNPRSTVXY]\d[A-Z]\s?\d[A-Z]\d)/i', $code)) {
//        return TRUE;
//      }
//
//      case 'US':
//      if (preg_match('/(\d{5}[\s-]\d{4})|\d{5})/', $code)) {
//        return TRUE;
//      }
//
//    }
//  }
//  return FALSE;
//}
/**
 * Accept a Drupal node path
 *
 * @param string $path
 * @return Boolean
 */
function valid_node_address($path) {
  return preg_match('/[A-Za-z0-9][\\/A-Za-z0-9]*/', $path);
}

/**
 * Tests for URLs in the form http://www.some.thing/... or 245.67.89.3
 */
//function valid_urlx($url) {
//  $url = trim($url);
//  if (preg_match('~(http|https)://[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?~i')) {
//    return TRUE;
//  }
//  elseif (preg_match('~(http|https)://(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)~')) {
//    return TRUE;
//  }
//  return FALSE;
//}
//

/**
 * drupal_goto but for external domains
 *
 * @param string $url
 */
function eactions_goto_url($url) {
  // Before the redirect, allow modules to react to the end of the page request.
  module_invoke_all('exit', '');
//  module_invoke_all('exit', $url);
  header('Location: '. $url);
  exit();
}

/**
 * Use CiviCRM's PEAR mail module to send a message
 *
 * @param string $body
 * @param array $headers
 * @param array attachment file info
 */
function eactions_mail($body, $headers, $attachments = NULL) {
  $config     =& CRM_Core_Config::singleton();
  /* @var $mailer Mail */
  $mailer     =& $config->getMailer();

  $message    =& new Mail_Mime("\n");
  $message->setTxtBody($body);
  //$message->setHTMLBody($html);
  if (is_array($attachments)) {
    foreach ($attachments as $file) {
      $message->addAttachment($file->filepath,$file->filemime,$file->filename);
    }
  }

  $b = $message->get(array('text_charset' => 'utf-8', 'html_charset' => 'utf-8', 'head_charset' => 'utf-8'));
  $h = $message->headers($headers);

  //        PEAR::setErrorHandling( PEAR_ERROR_CALLBACK,array('CRM_Mailing_BAO_Mailing', 'catchSMTP'));
 // $isOk = $mailer->send($headers['To'], $h, $b);
	$header = "From: ".$headers['From'];	

  $isOk = mail($headers['To'], $headers['Subject'], $body, $header);
  return ($isOk === TRUE);

}


/**
 * Used to bring in CiviCRM.Contact API when needed
 * - works arounds CiviCRM 1.2 dependency issues in a tidy way
 * @access public
 */
function load_crm_contact() {
//  global $civicrm_root;
//  require_once($GLOBALS['civicrm_root'].'/CRM/Core/Config.php');
//  require_once($GLOBALS['civicrm_root'].'/CRM/Core/PseudoConstant.php');
//  require_once($civicrm_root.'/api/utils.php');
//  require_once($civicrm_root.'/CRM/Core/BAO/UFMatch.php');
//  require_once($civicrm_root.'/CRM/Contact/BAO/Contact.php');
//  require_once($civicrm_root.'/api/Contact.php');
  civicrm_initialize(true);
//  module_invoke('civicrm','initialize',TRUE);
}


//function _target_try_to_target_address($email) {
//
// // This function is based on one that was written by
// // Jon S. Stevens jon AT clearink.com
// // Copyright 1998 Jon S. Stevens, Clear Ink
// // This code has all the normal disclaimers.
// // It is free for any use, just keep the credits intact.
//
//$server_name = $_SERVER['SERVER_NAME'];
//
//$rc = "Undefined error";
//
//list ( $user, $domain )  = split ( "@", $email, 2 );
//$arr = explode ( ".", $domain );
//$count = count ( $arr );
//$tld = $arr[$count - 2] . "." . $arr[$count - 1];
//
//if ( !$tld ) {
//   $rc = "Error: Invalid domain part in e-mail";
//   return $rc;
//   }
//
//if ( checkdnsrr ( $tld, "MX" ) ) {
//  if ( getmxrr ( $tld, $mxhosts, $weight ) ) {
//    for ( $i = 0; $i < count ( $mxhosts ); $i++ ) {
//      $fp = fsockopen ( $mxhosts[$i], 25 );
//      if ( $fp ) {
//        $s = 0;
//        $c = 0;
//        $out = "";
//        set_socket_blocking ( $fp, false );
//        do {
//          $out = fgets ( $fp, 2500 );
//          if ( ereg ( "^220", $out ) ) {
//            $s = 0;
//            $out = "";
//            $c++;
//          }
//          else
//          if ( ( $c > 0 ) && ( $out == "" ) ) {
//            break;
//          }
//          else {
//            $s++;
//          }
//          if ( $s == 9999 ) {
//            break;
//          }
//        } while ( $out == "" );
//
//        set_socket_blocking ( $fp, true );
//
//        fputs ( $fp, "HELO $server_name\n" );
//        $output = fgets ( $fp, 2000 );
//        fputs ( $fp, "MAIL FROM: <info@" . $tld . ">\n" );
//        $output = fgets ( $fp, 2000 );
//        fputs ( $fp, "RCPT TO: <$email>\n" );
//        $output = fgets ( $fp, 2000 );
//        if ( ereg ( "^250", $output ) ) {
//          $rc = "";
//        }
//        else {
//          $rc = $output;
//        }
//
//        fputs ( $fp, "QUIT\n" );
//        fclose( $fp );
//
//        if ($rc == "") {
//          break;
//          }
//        }
//      }
//    }
//  }
//return $rc;
//}
?>
